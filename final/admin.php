
<?php 
    
    require_once('templates/header.php');

    require 'conexion/conexion.php';

    $sql="SELECT * FROM oficina WHERE estado_oficina=0";
    $resultado1 = $mysqli->query($sql);

    $sql_oficina = "SELECT COUNT(*) FROM oficina WHERE estado_oficina=0";
        $resultado = $mysqli->query($sql_oficina);
        $fila = $resultado->fetch_row();
        $oficinas=$fila[0];



?>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">S.O.C.P. <small>Administración Usuarios</small></h1>
            </div>
        </div>
        <div class="container-fluid">
            <ul class="nav nav-tabs nav-justified"  style="font-size: 17px;">
                <li role="presentation"  class="active"><a href="admin.html">Administradores</a></li>
                <li role="presentation"><a href="teacher.html">.</a></li>
                <li role="presentation"><a href="student.html">.</a></li>
                <li role="presentation"><a href="personal.html">.</a></li>
            </ul>
        </div>
        <div class="container-fluid"  style="margin: 50px 0;">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <img src="assets/img/user01.png" alt="user" class="img-responsive center-box" style="max-width: 110px;">
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 text-justify lead">
                    Bienvenido a la sección para registrar nuevos administradores del sistema, debes de llenar todos los campos del siguiente formulario para registrar un administrador
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 lead">
                    <ol class="breadcrumb">
                      <li class="active">Nuevo administrador</li>
                      <li><a href="listadmin.html">Listado de administradores</a></li>
                    </ol>
                </div>
            </div>
        </div>
        
        <?php if($oficinas>0) { ?>

        <div class="container-fluid">
            <div class="container-flat-form">
                <div class="title-flat-form title-flat-blue">Registrar un nuevo administrador</div>
                <form action="usuario/guardar.php" method="POST" onsubmit="return validar(this)">
                    <div class="row">
                       <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                            <div class="group-material">
                                <input type="text" class="material-control tooltips-general" placeholder="Nombre completo" required="" maxlength="70" pattern="[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{1,70}" data-toggle="tooltip" data-placement="top" title="Escribe el nombre del administrador" name="nombre">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Nombre completo</label>
                            </div>
                           <div class="group-material">
                                <input type="text" class="material-control tooltips-general input-check-user" placeholder="apellidos del usuario" required="" maxlength="20" pattern="[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ]{1,20}" data-toggle="tooltip" data-placement="top" title="Escribe el Apellido del administrador" name="apellido" >
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Apellido Completo</label>
                           </div>
                            <div class="group-material">
                                <input type="number" class="material-control tooltips-general" placeholder="12345678"  maxlength="50" data-toggle="tooltip" data-placement="top" title="Ingrese el DNI" name="dni">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>DNI</label>
                            </div>
                            <div class="group-material">
                                <input type="email" class="material-control tooltips-general" placeholder="ejemplo@ejemplo.com"  maxlength="50" data-toggle="tooltip" data-placement="top" title="Ingrese el tipo de cargo" name="correo">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Correo</label>
                            </div> 
                             <div class="group-material">
                                <input type="text" class="material-control tooltips-general" placeholder="cargo"  maxlength="50" data-toggle="tooltip" data-placement="top" title="Ingrese el tipo de cargo" name="cargo">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Cargo</label>
                            </div>
                            <div class="group-material">
                                <span>Oficina</span>
                                <select class="material-control tooltips-general" data-toggle="tooltip" data-placement="top" title="Elige la especialidad" name="oficina">
                                    <option value="" disabled="" selected="">Selecciona una opción</option>
                                    <?php while($row = $resultado1->fetch_array(MYSQLI_ASSOC)) { ?>
                                        <option value="<?php echo $row['id_oficina']; ?>"> <?php echo $row['nombre_oficina']; ?>
                                            
                                        </option>
                                    <?php } ?>
                                    
                                </select>
                            </div>
                            <div class="group-material">
                                <input type="password" class="material-control tooltips-general" placeholder="Contraseña" required="" maxlength="200" data-toggle="tooltip" data-placement="top" title="Escribe una contraseña" id="contra" name="contra">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Contraseña</label>
                            </div>
                           <div class="group-material">
                                <input type="password" class="material-control tooltips-general" placeholder="Repite la contraseña" required="" maxlength="200" data-toggle="tooltip" data-placement="top" title="Repite la contraseña" id="contra2" name="contra2" >
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Repetir contraseña</label>
                           </div>
                            <p class="text-center">
                                <button type="reset" class="btn btn-info" style="margin-right: 20px;"><i class="zmdi zmdi-roller"></i> &nbsp;&nbsp; Limpiar</button>
                                <button type="submit" class="btn btn-primary"><i class="zmdi zmdi-floppy"></i> &nbsp;&nbsp; Guardar</button>
                            </p> 
                       </div>
                   </div>
                </form>
            </div>
        </div>
        <?php }else{ ?>
        <div class="container w-100 text-center">
            <h2> Para crear un centro de un <strong>usuario</strong> tiene que haber un centro de Producción disponible </h2>
            <h2><a  class="btn btn-primary" href="oficina.php">Crear Centro</a></h2>
        </div>    
        <?php } ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                </div>
                <div class="modal-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore dignissimos qui molestias ipsum officiis unde aliquid consequatur, accusamus delectus asperiores sunt. Quibusdam veniam ipsa accusamus error. Animi mollitia corporis iusto.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                </div>
            </div>
          </div>
        </div>
        
        <?php 
            require_once('templates/fooder.php')
        ?>

    </div>
    <script type="text/javascript">
        function valida(f) {
          var ok = true;
          var msg = "Escriba bien la contraseña :\n";
          if(document.getElementsByClassName("contra")[0].id != document.getElementsByClassName("contra2")[0].id)
          {

            msg += " iguales \n";
            ok = false;
          }

          return ok;
        }
</script>
</body>
</html>