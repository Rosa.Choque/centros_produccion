<?php
    require_once('templates/use_header.php');
?>


        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">Sistema Documentario <small> Financieros</small></h1>
            </div>
        </div>
        <div class="conteiner-fluid">
            <ul class="nav nav-tabs nav-justified"  style="font-size: 17px;">
                <li><a href="financiero_lista.php">Listar Documentos</a></li>
                <li class="active"><a href="loanpending.php">Creae Documento</a></li>
                <li><a href="loanreservation.html">Generacion Reporte</a></li>
            </ul>
        </div>
        <div class="container-fluid">
            <div class="container-flat-form">
                <div class="title-flat-form title-flat-blue">Ingresos Generados por Centro de Produccion por Mes</div>
                <form autocomplete="off" method="POST" action="financiero/guardar.php">
                    <div class="row">
                       <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                            <div class="group-material">
                                <input type="text" class="material-control tooltips-general" placeholder="Recibo" required="" maxlength="50" data-toggle="tooltip" data-placement="top" title="Escribe el numero de Recibo" name="numeroRecibo">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Numero de Recibo</label>
                            </div>
                            <div class="group-material">
                                <input type="date" class="material-control tooltips-general" placeholder="Fecha" required="" maxlength="50" data-toggle="tooltip" data-placement="top" title="Fecha" name="fecha">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Fecha de Recibo</label>
                            </div>
                            <div class="group-material">
                                <input type="text" class="material-control tooltips-general" placeholder="Monto S/." required="" maxlength="70" data-toggle="tooltip" data-placement="top" title="Escribe el monto del recibo" name="monto">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Monto</label>
                            </div>
                           <div class="group-material">
                                <input type="text" class="material-control tooltips-general" placeholder="Observaciones" required="" maxlength="70" data-toggle="tooltip" data-placement="top" title="Escribe el monto del recibo" name="observaciones">
                                <span class="highlight"></span>
                                <span class="bar"></span>
                                <label>Observaciones</label>
                            </div>
                        </div>
                    </div>  
                    <div class="w-100 text-center">
                        <button type="reset" class="btn btn-info" style="margin-right: 20px;"><i class="zmdi zmdi-roller"></i> &nbsp;&nbsp; Limpiar</button>
                    <button type="submit" class="btn btn-primary"><i class="zmdi zmdi-floppy"></i> &nbsp;&nbsp; Guardar</button>
                    </div>
                </form>   
            </div>
        </div>
        
        <?php require_once('templates/use_fooder.php'); ?>    

</body>
</html>