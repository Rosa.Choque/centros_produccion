
    <?php
        require_once('templates/header.php');

        require 'conexion/conexion.php';
        $sql_admin = "SELECT COUNT(*) as total FROM usuario WHERE id_tipo=1";
        $resultado = $mysqli->query($sql_admin);
        $fila = $resultado->fetch_row();
        $admin=$fila[0];
        

        $sql_user = "SELECT COUNT(*) FROM usuario WHERE id_tipo=2";
        $resultado = $mysqli->query($sql_user);
        $fila = $resultado->fetch_row();
        $user=$fila[0];


        $sql_oficina = "SELECT COUNT(*) FROM oficina";
        $resultado = $mysqli->query($sql_oficina);
        $fila = $resultado->fetch_row();
        $oficinas=$fila[0];

    ?>

    
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles">S.O.C.P. <small>Inicio</small></h1>
            </div>
        </div>
        <section class="full-reset text-center" style="padding: 40px 0;">
            <article class="tile">
                <div class="tile-icon full-reset"><i class="zmdi zmdi-face"></i></div>
                <div class="tile-name all-tittles">administradores</div>
                <div class="tile-num full-reset"><?php echo $admin; ?></div>
            </article>
            <article class="tile">
                <div class="tile-icon full-reset"><i class="zmdi zmdi-balance"></i></div>
                <div class="tile-name all-tittles">Oficinas</div>
                <div class="tile-num full-reset"><?php echo $oficinas; ?></div>
            </article>
            <article class="tile">
                <div class="tile-icon full-reset"><i class="zmdi zmdi-male-alt"></i></div>
                <div class="tile-name all-tittles">Encargados</div>
                <div class="tile-num full-reset"><?php echo $user; ?></div>
            </article>
            
           
            <article class="tile">
                <div class="tile-icon full-reset"><i class="zmdi zmdi-book"></i></div>
                <div class="tile-name all-tittles">Documentos Financieros</div>
                <div class="tile-num full-reset">77</div>
            </article>
            <article class="tile">
                <div class="tile-icon full-reset"><i class="zmdi zmdi-bookmark-outline"></i></div>
                <div class="tile-name all-tittles">Documentos Tecnicos </div>
                <div class="tile-num full-reset">11</div>
            </article>
            
            <article class="tile">
                <div class="tile-icon full-reset"><i class="zmdi zmdi-trending-up"></i></div>
                <div class="tile-name all-tittles" style="width: 90%;">reportes y estadísticas</div>
                <div class="tile-num full-reset">&nbsp;</div>
            </article>
        </section>
        <div class="modal fade" tabindex="-1" role="dialog" id="ModalHelp">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center all-tittles">ayuda del sistema</h4>
                </div>
                <div class="modal-body">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore dignissimos qui molestias ipsum officiis unde aliquid consequatur, accusamus delectus asperiores sunt. Quibusdam veniam ipsa accusamus error. Animi mollitia corporis iusto.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="zmdi zmdi-thumb-up"></i> &nbsp; De acuerdo</button>
                </div>
            </div>
          </div>
        </div>
       <?php require_once('templates/fooder.php'); ?>
    </div>
</body>
</html>